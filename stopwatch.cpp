// A simple stopwatch written in C++ using ncurses.
// Copyright (C) 2021 Austomus
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <ncurses.h>

int main() {
    int row, col;
    int ch = 0;
    int time = 0;

    // Initialize ncurses
    initscr();
    cbreak();
    noecho();
    curs_set(false);
    nodelay(stdscr, true);
    getmaxyx(stdscr, row, col);

    // Until the `q` key is pressed
    while (ch != 113) {
        int ms = time % 1000;
        int s = time / 1000 % 60;
        int m = time / 60000 % 60;
        int h = time / 3600000;

        // Print the stopwatch on the screen
        mvprintw(row / 2, (col - 12) / 2, "%02d:%02d:%02d.%03d", h, m, s, ms);
        refresh();
        // Wait for 1ms, and increment the `time` variable
        napms(1);
        ++time;

        // Check if the pressed key is SPACE
        if ((ch = getch()) == 32) {
            ch = 0;
            // Until the key that's pressed is SPACE
            while ((ch = getch()) != 32) {
                // If `q` is pressed
                if (ch == 113) {
                    // Leave ncurses mode, and exit
                    endwin();
                    return 0;
                }
            }
        }
    }

    // Leave ncurses mode, and exit
    endwin();
    return 0;
}
