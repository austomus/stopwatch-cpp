# stopwatch-cpp

A simple stopwatch written in C++ using ncurses.

## Building

```sh
mkdir build
cd build
cmake ..
make
```

## Contributing

1. Fork the repo
2. Make your changes
3. Push them
4. Open a pull request


